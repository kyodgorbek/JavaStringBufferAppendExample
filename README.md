# JavaStringBufferAppendExample



public class JavaStringBufferAppendExample {

 public static void main(String[] args){
 
 //StringBuffer append(boolean b) method appends boolean to StringBuffer object
 boolean b = true;
 StringBuffer sb1 = new StringBuffer("BooleanAppended :");
 sb1.append(b);
 System.out.println(sb1);
 
 //StringBuffer append(char c) method appends character to StringBuffer object
 char c = 'Y';
 StringBuffer sb2 = new StringBuffer("CharAppended");
 sb2.append(c);
 System.out.println(sb2);
 
 char[] c1 = new char[]{'Y', 'e', 's'};
 StringBuffer sb3 = new StringBuffer("Character Array Appended:");
 sb3.append(c1);
 System.out.println(sb3);
 
 //StringBuffer append(double d) method appends double to StringBuffer object
 float f = 1.0f;
 StringBuffer sb5 = new StringBuffer("floatAppended : ");
 sb5.append(f);
 System.out.println(sb5);
 
 //StringBuffer append(int i) method appends integer to StringBuffer object
 int i = 1;
 StringBuffer sb6 = new StringBuffer("integerAppended : ");
 sb6.append(i);
 System.out.println(sb6);
 
 //StringBuffer append(long i) method appends integer to StringBuffer object
 int I = 1;
 StringBuffer sb7 = new StringBuffer("longAppended : ");
 sb7.append(i);
 System.out.println(sb7);
 
   Object obj = new String("Yes");
   StringBuffer sb8 = new StringBuffer("ObjectAppended : ");
   sb8.append(obj);
   System.out.println("Yes");
   sb8.append(obj);
   System.out.println(sb8);
  }
 } 
 
